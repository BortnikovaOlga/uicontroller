<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<form action="${pageContext.request.contextPath}/addprocess">
    <h3>Добавить новый процесс</h3>
    <br>
    <div>
        <label for="processtype">Тип процесса</label>
        <select name="processtype" class="form-control" id="processtype">
            <c:forEach var="ptype" items="${processTypeList}">
                <option>${ptype.name}</option>
            </c:forEach>
        </select>
    </div>

    <div>
        <label for="description">Описание документа</label>
        <input type="text" name="description" class="form-control" id="description" pattern="^[А-Яа-яЁё\s]+$" maxlength="300" required/>
    </div>
    <div>
        <label for="date">Дата</label>
        <input type="datetime-local" name="date" class="form-control" id="date" required/>
    </div>
    <br>

    <button type="submit" class="btn btn-primary">Добавить</button>
    <!--button type="button" class="btn btn-primary">Отмена</button-->
</form>
</body>

