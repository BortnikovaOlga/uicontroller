<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>

<table class="table">
    <thead>
    <tr>
        <th>ID</th>
        <th>fio</th>
        <th>email</th>
        <th>telegram</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${userWithoutPasswordList}">
        <tr>
            <td scope="row">${user.idUser}</td>
            <td>${user.fio}</td>
            <td>${user.login}</td>
            <td>${user.email}</td>
            <td>${user.telegramChatId}</td>
            <%--td><a href="${pageContext.request.contextPath}/showuser?id=${user.getIdUser()}">Link</a></td--%>
        </tr>
    </c:forEach>
    </tbody>
</table>

<br>
<a href="/home/">На главную</a>

</body>
</html>
