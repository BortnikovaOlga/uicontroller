package ru.quick.approval.system.service;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.quick.approval.system.model.Process;
import ru.quick.approval.system.service.iservice.IProcessService;
import java.sql.Timestamp;

@Component
public class ProcessService implements IProcessService {

    @Override
    public boolean addProcess(int idtype, String description, String date) {

        Process process=new Process();
        process.setProcessTypeId(idtype);
        process.setDescription(description);
        System.out.println(date); // эхо временно
        process.setDateEndPlanning(Timestamp.valueOf(date.substring(0,9)+" "+date.substring(11,15)+":00.0"));
        process.setUserStartId(4); // временно заполняем пользователя с id=4

        System.out.println(process.toString()); // эхо временно

        /* вот это не проходит из-за нул полей*/
        new RestTemplate().postForObject("http://localhost:8080/process/process_type/{id}",process,Process.class,process.getProcessTypeId());
        return true;
    }
}
