package ru.quick.approval.system.service.iservice;

import ru.quick.approval.system.model.UserWithoutPassword;

public interface IUsersService {

    UserWithoutPassword[] allUsers();
}
