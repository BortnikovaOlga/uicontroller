package ru.quick.approval.system.service;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.quick.approval.system.model.ProcessType;
import ru.quick.approval.system.service.iservice.IProcessTypesService;

@Component
public class ProcessTypesService implements IProcessTypesService {

    @Override
    public ProcessType[] allProcessTypes() {
        return new RestTemplate().getForObject("http://localhost:8080/type",ProcessType[].class);
    }

}
