package ru.quick.approval.system.service.iservice;

import ru.quick.approval.system.model.ProcessType;

public interface IProcessTypesService {

    ProcessType[] allProcessTypes();
}
