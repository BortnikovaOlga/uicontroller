package ru.quick.approval.system.service;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.quick.approval.system.model.UserWithoutPassword;
import ru.quick.approval.system.service.iservice.IUsersService;

@Component
public class UsersService implements IUsersService {
    @Override
    public UserWithoutPassword[] allUsers() {
        return new RestTemplate().getForObject("http://localhost:8080/user",UserWithoutPassword[].class);
    }
}
