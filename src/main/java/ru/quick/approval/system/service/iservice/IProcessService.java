package ru.quick.approval.system.service.iservice;

import ru.quick.approval.system.model.Process;

public interface IProcessService {
    boolean addProcess(int idtype, String description, String date);
}
