package ru.quick.approval.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ru.quick.approval.system.model.UserWithoutPassword;
import ru.quick.approval.system.service.UsersService;

import java.util.Arrays;
import java.util.List;


@Controller
public class UsersController {

    @Autowired
    UsersService usersService;

    @RequestMapping(value="allusers")
    public String showUsers(Model model){

        List<UserWithoutPassword> listusers=Arrays.asList(usersService.allUsers());
            model.addAttribute(listusers);

        model.addAttribute("PageTitle", "Пользователи");
        model.addAttribute("PageBody", "allusers.jsp");
        return "layout";

    }

}
