package ru.quick.approval.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.quick.approval.system.model.ProcessType;
import ru.quick.approval.system.service.ProcessService;
import ru.quick.approval.system.service.ProcessTypesService;

import java.util.*;

/**
 * Контролер добавления нового Процесса
 * @author Bortnikova Olga
 *
 */

@Controller
public class ProcessController {
/// карта типов процессов, используется для поиска по названию id типа процесса
    private static Map<String, Integer> typeProcessMap;

    ProcessTypesService processTypesService;

    ProcessService processService;

    @Autowired
    public ProcessController(ProcessService processService, ProcessTypesService processTypesService) {
        this.processService = processService;
        this.processTypesService = processTypesService;
    }

    /**
     * метод контроля формы добавления нового Процесса, заполняет список и карту типов процессов, вызывает форму
     * @param model используется для добавления параметров формы
     * @return имя базовой формы
     */
    @RequestMapping(value = "addprocessform")
    public String addProcessForm(Model model) {

        List<ProcessType> listprocesstypes = Arrays.asList(processTypesService.allProcessTypes());

        typeProcessMap = new HashMap<>();
        for (ProcessType pt : listprocesstypes) {
            typeProcessMap.put(pt.getName(), pt.getIdProcessType());
        }
        model.addAttribute(listprocesstypes);
        model.addAttribute("PageTitle", "Добавить процесс");
        model.addAttribute("PageBody", "addprocessform.jsp");
        return "layout";
    }

    /**
     * метод контроля добавления нового процесса, принимает параметры от формы ,
     * вызывает сервис добавления нового процесса
     * @param processType выбранный пользователем тип процесса
     * @param description заполненное пользователем описание процесса
     * @param date заполненная пользователем дата окончания процесса
     * @return временно перекидывает на домашнюю страницу
     */
    @RequestMapping(value = "addprocess")
    public String addProcess(@RequestParam("processtype") String processType,
                             @RequestParam("description") String description,
                             @RequestParam("date") String date) {

        Integer idType = 0;
        if (typeProcessMap != null) idType = typeProcessMap.get(processType);

        processService.addProcess(idType, description, date);

        return "index";
    }
}
